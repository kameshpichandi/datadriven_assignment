package com.datadriventest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RegistationUsingApachePoi {

	public static void main(String[] args) throws IOException {
		
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		
		File f=new File("/home/kamesh/eclipse-workspace/Selenium_Assignments/ExcelData/TestData2.xlsx");
		FileInputStream fs=new FileInputStream(f);
		XSSFWorkbook workbook=new XSSFWorkbook(fs);
		XSSFSheet sheet = workbook.getSheetAt(1);
		int rows = sheet.getPhysicalNumberOfRows();
		
		for(int i=1;i<rows;i++) {
			
			String firstname = sheet.getRow(i).getCell(0).getStringCellValue();
			String lastName = sheet.getRow(i).getCell(1).getStringCellValue();
			String email = sheet.getRow(i).getCell(2).getStringCellValue();
			String password = sheet.getRow(i).getCell(3).getStringCellValue();
			String conformPassword = sheet.getRow(i).getCell(3).getStringCellValue();
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastName);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(conformPassword);
			driver.findElement(By.id("register-button")).click();
			driver.findElement(By.xpath("//a[normalize-space()='Log out']")).click();
			
			

			
		}

	}

}
