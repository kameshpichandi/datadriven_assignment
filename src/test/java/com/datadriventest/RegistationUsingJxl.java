package com.datadriventest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegistationUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");

		File fl = new File("/home/kamesh/eclipse-workspace/Selenium_Assignments/ExcelData/TestData1.xls");
		FileInputStream fs = new FileInputStream(fl);
		Workbook book = Workbook.getWorkbook(fs);
		Sheet sheet = book.getSheet("Registation");
		

		int rows = sheet.getRows();
		for(int i=3;i<rows;i++) {
			
			String firstName = sheet.getCell(0,i).getContents();
			String lastName = sheet.getCell(1,i).getContents();
			String email = sheet.getCell(2,i).getContents();
			String password = sheet.getCell(3,i).getContents();
			String conformPass = sheet.getCell(3,i).getContents();
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstName);
			driver.findElement(By.id("LastName")).sendKeys(lastName);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(conformPass);
			driver.findElement(By.id("register-button")).click();
			driver.findElement(By.linkText("Log out")).click();
			
			
		}
		
		

	}

}
